package com.binary_studio.uniq_in_sorted_stream;

import java.util.function.Predicate;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {

		return stream.filter(new Predicate<Row<T>>() {
			private Row<T> previous;
			@Override
			public boolean test(Row<T> tRow) {
				if (tRow.equals(previous))
					return false;
				previous = tRow;
				return true;
			}
		});
	}

}
