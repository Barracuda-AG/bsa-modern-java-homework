package com.binary_studio.tree_max_depth;

import java.util.*;
import java.util.stream.Collectors;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
			Queue<Department> queue = new ArrayDeque<>();
			int levelsCount = 0;

			if(rootDepartment == null) return 0;

			queue.add(rootDepartment);

			while(!queue.isEmpty()){
			levelsCount++;
			int size = queue.size();

			for(int i = 0; i < size; i++){
				Department current = queue.poll();
					queue.addAll(current.subDepartments.stream().filter(Objects::nonNull).collect(Collectors.toList()));
				}
			}
		return levelsCount;
	}
}

