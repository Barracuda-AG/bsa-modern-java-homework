package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {
	private DefenciveSubsystemBuilder defenciveSubsystemBuilder;

	public DefenciveSubsystemImpl(String name, PositiveInteger powergridConsumption, PositiveInteger capacitorConsumption,
								  PositiveInteger impactReductionPercent, PositiveInteger shieldRegeneration,
								  PositiveInteger hullRegeneration){
		defenciveSubsystemBuilder = DefenciveSubsystemBuilder.named(name)
				.pg(powergridConsumption.value())
				.capacitorUsage(capacitorConsumption.value())
				.impactReduction(impactReductionPercent.value())
				.shieldRegen(shieldRegeneration.value())
				.hullRegen(hullRegeneration.value());
	}
	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {

		if(name.isBlank()) throw new IllegalArgumentException("Name should be not null and not empty");
		return new DefenciveSubsystemImpl(name,powergridConsumption,capacitorConsumption,impactReductionPercent,
				shieldRegeneration,hullRegeneration);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {

		return defenciveSubsystemBuilder.getPgRequirement();
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {

		return defenciveSubsystemBuilder.getCapacitorUsage();
	}

	@Override
	public String getName() {

		return defenciveSubsystemBuilder.getName();
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		int percent = defenciveSubsystemBuilder.getImpactReduction().value();
		if(percent > 95) percent = 95;
		int valueToReduce = (int)Math.floor(percent*incomingDamage.damage.value()/100.0);
		int damageAfterReduce = incomingDamage.damage.value() - valueToReduce;
		if(damageAfterReduce == 0) damageAfterReduce = 1;
		return new AttackAction(PositiveInteger.of(damageAfterReduce), incomingDamage.attacker, incomingDamage.target,incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {

		RegenerateAction regenerateAction = new RegenerateAction(defenciveSubsystemBuilder.getShieldRegen(),defenciveSubsystemBuilder.getHullRegen());
		return regenerateAction;
	}

}
