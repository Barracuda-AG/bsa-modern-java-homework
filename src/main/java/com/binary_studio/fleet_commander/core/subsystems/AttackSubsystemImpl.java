package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {
	private  AttackSubsystemBuilder attackSubsystemBuilder;

	public AttackSubsystemImpl(String name, PositiveInteger powergridRequirments,
							   PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
							   PositiveInteger baseDamage){
		attackSubsystemBuilder = AttackSubsystemBuilder.named(name)
		.pg(powergridRequirments.value())
		.capacitorUsage(capacitorConsumption.value())
		.optimalSpeed(optimalSpeed.value())
		.optimalSize(optimalSize.value())
		.damage(baseDamage.value());
	}
	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {

		if(name.isBlank()) throw new IllegalArgumentException("Name should be not null and not empty");

		return new AttackSubsystemImpl(name,powergridRequirments,capacitorConsumption,optimalSpeed,optimalSize,baseDamage);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {

		return attackSubsystemBuilder.getPgRequirement();
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {

		return attackSubsystemBuilder.getCapacitorUsage();
	}

	@Override
	public PositiveInteger attack(Attackable target) {


		double targetSize = target.getSize().value();
		double optimalSize = attackSubsystemBuilder.getOptimalSize().value();
		double targetSpeed = target.getCurrentSpeed().value();
		double optimalSpeed = attackSubsystemBuilder.getOptimalSpeed().value();

		double sizeReductionModifier = (targetSize >= optimalSize) ? 1 : (targetSize/optimalSize);
		double speedReductionModifier = (targetSpeed <= optimalSpeed) ? 1 : (optimalSpeed/(2*targetSpeed));
		int damage = (int)Math.ceil(attackSubsystemBuilder.getBaseDamage().value() * Double.min(sizeReductionModifier, speedReductionModifier));
		return PositiveInteger.of((damage == 0)? 1 : damage);
	}

	@Override
	public String getName() {

		return attackSubsystemBuilder.getName();
	}

	public AttackSubsystemBuilder getAttackSubsystemBuilder() {
		return attackSubsystemBuilder;
	}
	public PositiveInteger getSize(){
		return attackSubsystemBuilder.getOptimalSize();
	}
	public PositiveInteger getSpeed(){
		return attackSubsystemBuilder.getOptimalSpeed();
	}
}
