package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {
	    private AttackSubsystem attackSubsystem;
		private DefenciveSubsystem defenciveSubsystem;
		DockedShipBuilder dockedShipBuilder;
		private int powerGrid = 0;



	public DockedShip(String name, PositiveInteger shieldHP,PositiveInteger hullHP,
					  PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate){
		dockedShipBuilder = DockedShipBuilder.named(name)
		.shield(shieldHP.value())
		.hull(hullHP.value())
		.pg(powergridOutput.value())
		.capacitor(capacitorAmount.value())
		.capacitorRegen(capacitorRechargeRate.value());

	}
	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate) {

		return new DockedShip(name,shieldHP,hullHP,powergridOutput,capacitorAmount,capacitorRechargeRate);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {

		if(subsystem == null) attackSubsystem = null;
		else if(dockedShipBuilder.getPg().value() - powerGrid >= subsystem.getPowerGridConsumption().value()) {
			attackSubsystem = subsystem;
			powerGrid += subsystem.getPowerGridConsumption().value();
		}
				else
					throw new InsufficientPowergridException(subsystem.getPowerGridConsumption().value());
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {

		if(subsystem == null) defenciveSubsystem = null;
		else if(dockedShipBuilder.getPg().value() - powerGrid >= subsystem.getPowerGridConsumption().value()) {
			defenciveSubsystem = subsystem;
			powerGrid += subsystem.getPowerGridConsumption().value();
		}
		else
			throw new InsufficientPowergridException(subsystem.getPowerGridConsumption().value());
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if(this.isBothSubsystemEmpty()) throw NotAllSubsystemsFitted.bothMissing();
		else if(this.isAttackSubsystemEmpty()) throw NotAllSubsystemsFitted.attackMissing();
		else if(this.isDefenciveSubsystemEmpty()) throw NotAllSubsystemsFitted.defenciveMissing();

		return new CombatReadyShip(this);
	}
	private boolean isBothSubsystemEmpty(){
		return (attackSubsystem == null && defenciveSubsystem == null);
	}
	private boolean isAttackSubsystemEmpty(){
		return (attackSubsystem == null);
	}
	private boolean isDefenciveSubsystemEmpty(){
		return (defenciveSubsystem == null);
	}

	public AttackSubsystem getAttackSubsystem() {
		return attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return defenciveSubsystem;
	}
}
