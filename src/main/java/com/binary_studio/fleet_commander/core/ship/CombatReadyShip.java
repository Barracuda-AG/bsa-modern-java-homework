package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;
	private PositiveInteger shieldHP;
	private PositiveInteger hullHP;
	private PositiveInteger capacitor;
	private PositiveInteger capacitorRegeneration;
	private PositiveInteger pg;
	private AttackSubsystem attackSubsystem;
	private DefenciveSubsystem defenciveSubsystem;
	private PositiveInteger optimalSize;
	private PositiveInteger optimalSpeed;
	private AttackAction attackAction;


		public CombatReadyShip(DockedShip dockedShip){
			name = dockedShip.dockedShipBuilder.getName();
			shieldHP = dockedShip.dockedShipBuilder.getShieldHP();
			hullHP = dockedShip.dockedShipBuilder.getHullHP();
			capacitor = dockedShip.dockedShipBuilder.getCapacitor();
			capacitorRegeneration = dockedShip.dockedShipBuilder.getCapacitorRegeneration();
			pg = dockedShip.dockedShipBuilder.getPg();
			attackSubsystem = dockedShip.getAttackSubsystem();
			defenciveSubsystem = dockedShip.getDefenciveSubsystem();

		}

	@Override
	public void endTurn() {
		regenerate();
	}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {

		return this.name;
	}

	@Override
	public PositiveInteger getSize() {

		return optimalSize;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {

		return optimalSpeed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {

		Optional<AttackAction> attackAction = Optional.of(new AttackAction(PositiveInteger.of(10),this,target,this));
		return attackAction;
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
			attackAction = attack;
		AttackResult attackResult = new AttackResult.DamageRecived(attack.weapon, attack.damage, attack.target);
		return attackResult;
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
	 return Optional.ofNullable(defenciveSubsystem.regenerate());

	}

}
